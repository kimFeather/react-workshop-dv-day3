import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View ,Alert} from 'react-native';
import { Link } from 'react-router-native';

class Screen1 extends Component {
    UNSAFE_componentWillMount(){
        console.log(this.props)
        if(this.props.location && this.props.location.state && this.props.location.state.mynumber ){
            Alert.alert('Your number is', this.props.location.state.mynumber+ '')
        }
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'blue' }}>
                <Text style={{ color: 'white' }}>Screen1</Text>
                <Link to="/screen2">
                    <Text style={{ color: 'white' }}> Go to Screen2</Text>
                </Link>
            </View>
        )
    }
}


export default Screen1