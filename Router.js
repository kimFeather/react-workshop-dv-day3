import React, { Component } from 'react';
import { Route, NativeRouter , Switch , Redirect} from 'react-router-native';
import LoginPage from './LoginPage'
import MainPage from './MainPage'
import ProfilePage from './ProfilePage'
import EditProfile from './EditProfilePage'
import AddProduct from './AddProduct'
import ProductPage from './ProductPage'
import EditProduct from './EditProduct'


class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path="/Login" component={LoginPage}/>
                    <Route exact path="/main" component={MainPage}/>
                    <Route exact path="/profile" component={ProfilePage}/>
                    <Route exact path="/editprofile" component={EditProfile}/>
                    <Route exact path="/addproduct" component={AddProduct}/>
                    <Route exact path="/product" component={ProductPage}/>
                    <Route exact path="/editproduct" component={EditProduct}/>

                    <Redirect to="/Login" />
                </Switch>
            </NativeRouter>
        )
    }
}


export default Router