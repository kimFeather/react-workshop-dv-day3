import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Button, TouchableOpacity } from 'react-native';



export default class ProductPage extends React.Component {

    state = {
        username: '',
        password: '',
        name: '',
        surname: '',
        dataLeft: [],
        dataRight: [],
        productClick: {key: '', pname: ''}
    }

    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.username && this.props.location.state.password) {
            this.setState({ username: this.props.location.state.username })
            this.setState({ password: this.props.location.state.password })
        }
        if (this.props.location && this.props.location.state && this.props.location.state.name && this.props.location.state.surname) {
            this.setState({ name: this.props.location.state.name })
            this.setState({ surname: this.props.location.state.surname })
        }
        if (this.props.location && this.props.location.state && this.props.location.state.dataLeft && this.props.location.state.dataRight) {
            this.setState({ dataLeft: this.props.location.state.dataLeft })
            this.setState({ dataRight: this.props.location.state.dataRight })
        }
        if (this.props.location && this.props.location.state && this.props.location.state.productClick) {
            this.setState({ productClick: this.props.location.state.productClick })
        }
    }

    navigateToMainPage = () => {
        this.props.history.replace('/main', this.state)
    }

    navigateToEditProduct = () => {
        this.props.history.replace('/editproduct', this.state)
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerSection}>
                    <TouchableOpacity style={[styles.squareButton]} onPress={this.navigateToMainPage}>
                        <Text style={{ color: 'white', fontSize: 25 }}>X</Text>
                    </TouchableOpacity>
                    <View style={styles.headerText}>
                        <Text style={{ fontSize: 24, color: 'white' }}>Product</Text>
                    </View>
                </View>
                <View style={styles.scrollContainer}>
                    <Image style={{width: 150, height: 150, marginBottom: 20}} source={{uri: "https://bit.ly/2N34MuD"}}></Image>
                    <Text style={{ color: 'black', fontSize: 18 }}>Product Name: {this.state.productClick.pname}</Text>
                    <TouchableOpacity
                        style={{
                            width: 140,
                            height: 40,
                            backgroundColor: 'gray',
                            marginTop: 40,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                        onPress={this.navigateToEditProduct}
                    >
                        <Text style={{ color: 'white', fontSize: 20 }}>Edit</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerSection: {
        height: 60,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    content: {
        backgroundColor: '#f7f7f7',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        height: 60,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    squareButton: {
        height: 60,
        width: 60,
        backgroundColor: '#8b9dc3',
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerText: {
        flex: 1,
        backgroundColor: '#3b5998',
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollContainer: {
        backgroundColor: '#dfe3ee',
        flex: 1,
        padding: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollContent: {
        flex: 1,
        flexDirection: 'row',
    },
    contentChild: {
        flex: 1,
    },
    box: {
        height: 180,
        backgroundColor: '#ffffff',
        margin: 10
    },
    boxText: {
        textAlign: 'center',
        fontSize: 20,
        marginTop: 16
    }
})


