import React from 'react';
import { StyleSheet, Text, View, ScrollView, Button, Modal, Image, TextInput, TouchableOpacity, Alert } from 'react-native';

export default class LoginPage extends React.Component {

    state = {
        username: '',
        password: '',
        modalVisible: false,
        isLogin: false
    }

    checkLogin = () => {
        this.setState({ isLogin: this.state.username === 'admin' && this.state.password === 'eiei' })
        this.setState({ modalVisible: true })
    }

    changeText = (field, text) => {
        this.setState({ [field]: text })
    }

    goToProfile = () => {
        this.props.history.push('/main', {
            mynumber: 20
        })
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topSection}>
                    <Image
                        style={styles.image}
                        source={{ uri: 'https://t1.kn3.net/taringa/F/3/8/2/0/1/DavidXaphanDeRiv/4A6.png  ' }}
                    />

                </View>
                <View style={styles.bottSection}>
                    <View style={styles.bottSectionChild1}>

                        <TextInput
                            style={styles.textInput}
                            placeholder="Username"
                            value={this.state.username}
                            onChangeText={(value) => { this.changeText('username', value) }}
                        />
                        <TextInput style={styles.textInput}
                            placeholder="Password"
                            value={this.state.password}
                            onChangeText={(value) => { this.changeText('password', value) }}
                        />

                    </View>
                    <View style={styles.bottSectionChild2}>
                        <TouchableOpacity
                            title='Login'
                            style={styles.touchableOpacity}
                            onPress={this.checkLogin}>
                            <Text style={{ fontSize: 24, color: 'white' }}>Login</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}
                    style={{ alignItems: 'center', flex: 1 }}>
                    <View style={styles.modalBox}>
                        <View style={styles.modalContent}>
                            <Text style={{ fontSize: 26, textAlign: 'center', color: 'white' }}>
                                {(this.state.isLogin === true ? 'Login Sucess' : 'Wrong Username or Password')}
                            </Text>

                            <TouchableOpacity
                                // onPress={() => {
                                //     this.setState({ modalVisible: false });
                                //     this.setState({ isLogin: false })
                                //     this.goToProfile
                                // }}

                                onPress={this.goToProfile}
                                style={styles.hideModal}
                            >
                                <Text style={{ fontSize: 20, textAlign: 'center', color: 'white' }}>Hide Modal</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'steelblue'
    },
    topSection: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    },
    bottSection: {
        flex: 1,
        alignItems: 'center'
    },
    bottSectionChild1: {
        flex: 1,
    },
    bottSectionChild2: {
        flex: 1,
        justifyContent: 'center'
    },
    image: {
        width: 200,
        height: 200,
        borderRadius: 100,
        backgroundColor: 'white'
    },
    textInput: {
        width: 350,
        height: 50,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'powderblue',
        marginBottom: 20,
        fontSize: 24
    },
    touchableOpacity: {
        width: 350,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'skyblue'
    },
    modalBox: {
        margin: '2.5%',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        borderRadius: 10
    },
    modalContent: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    hideModal: {
        marginTop: 30,
        backgroundColor: 'grey',
        width: 150,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
